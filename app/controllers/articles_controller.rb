class ArticlesController < ApplicationController
  before_action :set_article, only: %i[ show edit update destroy ]
  before_action :set_categories, only: %i[ new create edit update ]

  def index
    @articles = Article.accessible_by(current_ability)

    if category = Category.find_by(key: params[:category_key])
      @articles = @articles.where(category: category)
    end
  end

  def show
    authorize! :read, @article
  end

  def new
    @article = Article.new
    authorize! :create, @article
  end

  def edit
    authorize! :edit, @article
  end

  def create
    @article = current_user.articles.build(article_params)
    authorize! :create, @article

    respond_to do |format|
      if @article.save
        format.html { redirect_to article_url(@article), notice: "Article was successfully created." }
        format.json { render :show, status: :created, location: @article }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    authorize! :edit, @article

    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to article_url(@article), notice: "Article was successfully updated." }
        format.json { render :show, status: :ok, location: @article }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    authorize! :destroy, @article

    @article.destroy

    respond_to do |format|
      format.html { redirect_to articles_url, notice: "Article was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private

  def set_article
    @article = Article.friendly.find(params[:id])
  end

  def article_params
    params.require(:article).permit(:title, :body, :preview_img, :category_id)
  end

  def set_categories
    @categories_array = Category.categories_list.pluck(:title, :id)
  end
end
