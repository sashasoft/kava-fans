module Categories::SortedCollection
  def categories_list
    Category.where.not(key: 'inshe') + Category.where(key: 'inshe')
  end
end