class Article < ApplicationRecord
  extend FriendlyId

  belongs_to :author, class_name: 'User', foreign_key: :author_id
  belongs_to :category

  has_rich_text    :body

  has_one_attached :preview_img

  validates :body, presence: true
  validates :title, presence: true
  validates :preview_img, presence: true

  friendly_id :title, use: [:slugged, :finders]

  def should_generate_new_friendly_id?
    slug.blank? || title_changed?
  end
end
