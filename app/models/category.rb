class Category < ApplicationRecord
  extend Categories::SortedCollection

  before_create :set_key
  has_many :articles

  private

  def set_key
    self.key = I18n.transliterate(self.title).parameterize
  end
end
