Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  resources :articles
  devise_for :users

  root "articles#index"

  get 'about', to: 'welcome#about'
  get 'news', to: 'articles#index'
end
