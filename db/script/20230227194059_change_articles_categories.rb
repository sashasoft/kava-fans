class ChangeArticlesCategories 
  def self.call
    if Category.exists?(key: "inshe")
      find_key = 'inshe'
    elsif Category.exists?(key: "other")
      find_key = 'other'
    else
      category = Category.create(title: 'Інше')
      find_key = category.key
    end

    other_category = Category.find_by(key: find_key)

    articles = Article.where(category_id: nil)
    articles.update_all(category_id: other_category.id)
  end
end