class AddKeyToCategory < ActiveRecord::Migration[7.0]
  def change
    add_column :categories, :key, :string
    add_index :categories, :key,  unique: true
  end
end
